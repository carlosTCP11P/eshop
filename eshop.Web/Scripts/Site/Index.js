﻿//Cargar los productos en la pagina principal
window.onload = function cargarDatos() {
    var url = 'Products/GetAll'
    Common.Ajax('GET', url, null, 'html', insertData);
}
//Funcion que se ejecuta al terminar de cargar los productos si todo ha ido bien
function insertData(result) {
    $('#ListProds').html(result);
    console.log("Los datos se han recuperado correctamente.");
}

$('#form').submit(function (e) {
    // e.preventDefalt();

    data = Common.ConvertToJson(this);



    Common.Ajax("POST", "Products/PostProduct", data, 'html', doneData);

    return false;
});
function readData() {

    var getForm = document.getElementById("form");


    getForm.addEventListener("submit", function (e) {
        // e.preventDefalt();
        console.log("Le hemos dado el click")
        var data = new FormData(getForm);


        Common.Ajax("POST", "Products/PostProduct", data, 'html', doneData);

        console.log("name");

    })

    ////var name = document.getElementById('username').value;
    ////var description = document.getElementById('description').value;
    ////var priceproduct = document.getElementById('priceproduct');
    ////var stock = document.getElementById('stock');
    ////var image = document.getElementById('image');
    ////console.log(username)
}
function doneData() {

}
//Funcion añadir al carrito
function addToCart(id) {
    var url;
    var method;
    
    if (localStorage.getItem('orderCreated') == undefined || localStorage.getItem('orderCreated')=="false") {
         url = 'Orders/CreateOrder';
         method='Post'
    } else {
        url = 'Orders/UpdateOrder';
        method='put'
    }
    var data = {};
    data['productId'] = id;
    Common.Ajax(method, url, data, 'json', createdOrder);
    
}
//Creacion de nuestro pedido
function createdOrder(result) {
    
    if (result.redirecturl != null) {
        localStorage.setItem('orderCreated', 'false');
        window.location.href = result.redirecturl;
    } else {
        localStorage.setItem('orderCreated', 'true');
        $('.badge-count').html(result.count);
    }

}