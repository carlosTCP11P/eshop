﻿var promoCode;
var promoPrice;
var fadeTime = 300;

/* Acciones */
$('.quantity input').change(function () {
    updateQuantity(this);
});

$('.remove button').click(function () {
    removeItem(this);
});

$(document).ready(function () {
    updateSumItems();
});

/* Funcion para recalcular carrito */
function recalculateCart(onlyTotal) {
    var subtotal = 0;

    /* Sum up row totals */
    $('.basket-product').each(function () {
        subtotal += parseFloat($(this).children('.subtotal').text());
    });

    /* Calculate totals */
    var total = subtotal;

    //If there is a valid promoCode, and subtotal < 10 subtract from total
    var promoPrice = parseFloat($('.promo-value').text());
    if (promoPrice) {
        if (subtotal >= 10) {
            total -= promoPrice;
        } else {
            alert('Order must be more than £10 for Promo code to apply.');
            $('.summary-promo').addClass('hide');
        }
    }

    /*If switch for update only total, update only total display*/
    if (onlyTotal) {
        /* Update total display */
        $('.total-value').fadeOut(fadeTime, function () {
            $('#basket-total').html(total.toFixed(2));
            $('.total-value').fadeIn(fadeTime);
        });
    } else {
        /* Update summary display. */
        $('.final-value').fadeOut(fadeTime, function () {
            $('#basket-subtotal').html(subtotal.toFixed(2));
            $('#basket-total').html(total.toFixed(2));
            if (total == 0) {
                $('.checkout-cta').fadeOut(fadeTime);
            } else {
                $('.checkout-cta').fadeIn(fadeTime);
            }
            $('.final-value').fadeIn(fadeTime);
        });
    }
}

/* Update quantity */
function updateQuantity(quantityInput) {
    /* Calculate line price */
    var productRow = $(quantityInput).parent().parent();
    var price = parseFloat(productRow.children('.price').text());
    var data = productRow.children('.remove').data();
    var quantity = $(quantityInput).val();
    var linePrice = price * quantity;
    data['quantity'] = quantity;

    Common.Ajax('PUT', 'Orders/UpdateLine', data, 'json', updateLineProduct);

    function updateLineProduct() {
        /* Update line price display and recalc cart totals */
        productRow.children('.subtotal').each(function () {
            $(this).fadeOut(fadeTime, function () {
                $(this).text(linePrice.toFixed(2));
                recalculateCart();
                $(this).fadeIn(fadeTime);
            });
        });

        productRow.find('.item-quantity').text(quantity);
        updateSumItems();
    }
}

function updateSumItems() {
    var sumItems = 0;
    $('.quantity input').each(function () {
        sumItems += parseInt($(this).val());
    });
    $('.total-items').text(sumItems);
}

/* Remove item from cart */
function removeItem(removeButton) {
    /* Remove row from DOM and recalc cart total */
    var productRow = $(removeButton).parent().parent();
    var data = $(removeButton).parent().data();

    Common.Ajax('DELETE', 'Orders/DeleteLine', data, 'json', deleteLineProduct);

    function deleteLineProduct(result) {
        productRow.slideUp(fadeTime, function () {
            $('.badge-count').html(result.count);
            productRow.remove();
            recalculateCart();
            updateSumItems();
        });
    }
}

$('#formCard').card({
    container: '.card-wrapper',
    width: 280,

    formSelectors: {
        nameInput: 'input[name="first-name"], input[name="last-name"]'
    }
});

$('#formCard').submit(function (e) {

    data = Common.ConvertToJson(this);
    var correct=validateCardNumber(data['number']);

    var correct2= validateExpireDate(data['expiry']);
    if (!correct || !correct2) {
        return false;
    }
    Common.Ajax('PUT', 'Orders/FinishOrder', null, 'json', orderFinished);
    return false;
});


function validateCardNumber(cardNumber) {
    cardNumber = cardNumber.replace(/ /g, "");
    var americanExpress = /^(?:3[47][0-9]{13})$/;
    var visa = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/;
    var masterCard = /^(?:5[1-5][0-9]{14})$/;
    var discoverCard = /^(?:6(?:011|5[0-9][0-9])[0-9]{12})$/;
    if (cardNumber.match(americanExpress)) {
        return true;
    }
    else {
        if (cardNumber.match(visa)) {
            return true;
        } else {
            if (cardNumber.match(masterCard)) {
                return true;
            } else {
                if (cardNumber.match(discoverCard)) {
                    return true;
                } 
            }
        }
        alert("Not a valid card number!");
        return false;
    }
}

function validateExpireDate(date) {
    date = date.replace(/ /g, "");
    dateSplitted = date.split("/");
    today = new Date();
    someday = new Date();
    someday.setFullYear('20' + dateSplitted[1], dateSplitted[0], 1);

    if (someday < today) {
        alert("The expiry date is before today's date. Please select a valid expiry date");
        return false;
    }
    return true
}


function orderFinished(result) {
    window.location.href = result.redirecturl;
    localStorage.removeItem('orderCreated');
}