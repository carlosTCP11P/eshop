﻿$('#').submit(function (e) {

    data = Common.ConvertToJson(this);
    if (data.Id == 0) {
        Common.Ajax("POST", "Products/PostProduct", data, 'html', dataInserted);
    } else {
        Common.Ajax("PUT", "Products/UpdateProduct", data, 'html', dataInserted);
    }

    return false;
});

function changeRole(id) {
    var data = {};
    data['id'] = id;
    Common.Ajax("POST", "Account/ChangeRole", data, 'json', reload);
}

function reload(result) {
    window.location.href = result;
}