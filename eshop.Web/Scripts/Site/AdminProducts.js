﻿$('#form').submit(function (e) {
    //Conversión a JSON POST y PUT de producto nuevo y actualizado
    data = Common.ConvertToJson(this);
    if (data.Id == 0) {
        Common.Ajax("POST", "Products/PostProduct", data, 'html', dataInserted);
    } else {
        Common.Ajax("PUT", "Products/UpdateProduct", data, 'html', dataInserted);
    }

    return false;
});
//Funciones de pag AdminProducts
function dataInserted() {
    window.onload();
    resetModal();
    console.log("Hola");
}
//Muestro los productos
function viewData(result) {

    $("#ListProds").html(result);
    console.log("Los datos solo los muestro");

}
//Reset del Modal
function resetModal() {
    $('#form')[0].reset();
    $('#exampleModal').modal('hide');
}
//Funcion AJAX
window.onload = function AJAX() {
    var url = 'AdminProducts/TableAll'//cadena de conex
    Common.Ajax('GET', url, null, 'html', viewData); //llamada a ajax

}
$('#exampleModal').on('hidden.bs.modal', function () {
    $('#form')[0].reset();
})
