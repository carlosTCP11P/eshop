﻿//Funcion AJAX
var Common = {
    Ajax: function (httpMethod, url, data, returnType, successCallBack) {
        var fullURL= 'http://localhost:60248/' + url;
        var ajaxObj = $.ajax({
            type: httpMethod.toUpperCase(),
            url: fullURL,
            data: data,
            dataType: returnType,
            success: successCallBack,
            error: function (error, type, httpStatus) {
                Common.AjaxFailureCallback(error, type, httpStatus);
            }
        });
        return ajaxObj
    },

    AjaxFailureCallback: function (error, type, httpStatus) {
        var failureMesage = "Error ocurred in ajax call:" + error.status + " - " + error.responserText + " - " + httpStatus;
        console.log(failureMesage);
    },
    //Conversión a JSON
    ConvertToJson: function (form) {
        var o = {};
        var a = $(form).serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    }



}
