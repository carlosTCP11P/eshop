﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(eshop.Web.Startup))]
namespace eshop.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
