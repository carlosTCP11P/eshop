﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eshop.Core;

namespace eshop.Web.Models
{
    public class IndexModel
    {
        public List<Product> products { get; set; }
    }
}