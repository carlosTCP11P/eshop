using System.Web.Mvc;
using eshop.Application;
using eshop.Application.Interface;
using eshop.DAL;
using eshop.Web.Controllers;
using Microsoft.Practices.Unity;
using Unity.Mvc3;

namespace eshop.Web
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            container.RegisterType<IProductsBL, ProductsBL>();
            container.RegisterType<IController, ProductsController>("Products");

            container.RegisterType<IOrdersBL, OrdersBL>();
            container.RegisterType<IController, OrdersController>("Orders");

            container.RegisterType<ILineOfOrdersBL, LineOfOrderBL>();
            container.RegisterType<IUsersBL, UsersBL>();

            container.RegisterType<AccountController>(new InjectionConstructor());
            container.RegisterType<ManageController>(new InjectionConstructor());
            //container.RegisterType<IOrdersBL, OrdersBL>();
            //container.RegisterType<IController, AccountController>("Account");

            container.RegisterType<IUnitOfWork, UnitOfWork>();

            return container;

        }
    }
}