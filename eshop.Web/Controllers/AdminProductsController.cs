﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eshop.Application.Interface;
using Microsoft.AspNet.Identity;

namespace eshop.Web.Controllers
{

    public class AdminProductsController : Controller
    {

        private IProductsBL ProductsBL { get; set; } //iniciamos la interfaz de la clase, declarada la variable 
        private IOrdersBL OrdersBL { get; set; }
        public IUsersBL UsersBL { get; set; }

        public AdminProductsController(IProductsBL productsBL, IOrdersBL ordersBL, IUsersBL usersBL) //nueva instancia igualada a la antigua
        {
            ProductsBL = productsBL;
            OrdersBL = ordersBL;
            UsersBL = usersBL;
        }

        // GET: AdminProducts
        public ActionResult Index()
        {
            return View();
        }

        // GET: AdminProducts/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: AdminProducts/Create
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult AdminProducts()
        {
            if (User.Identity.GetUserId() == null || User.IsInRole("Admin")==false)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public ActionResult AdminOrders()
        {
            if (User.Identity.GetUserId() == null || User.IsInRole("Admin") == false)
            {
                return RedirectToAction("Index", "Home");
            }
            var orders=OrdersBL.GetAllOrders();
            return View(orders);
        }

        public ActionResult AdminUsers()
        {
            if (User.Identity.GetUserId() == null || User.IsInRole("Admin") == false)
            {
                return RedirectToAction("Index", "Home");
            }
            var users = UsersBL.GetAllUsers();
            return View(users);
        }

        [HttpGet]
        public ActionResult TableAll()
        {
            var tableProducts = ProductsBL.GetAllProducts();
            return PartialView("_PartialPageAdminProducts", tableProducts);
            
        }

        [HttpGet]
        public ActionResult GetAllOrders()
        {
            var orders = OrdersBL.GetAllOrders();
            return PartialView("", orders);
        }

        // POST: AdminProducts/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: AdminProducts/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: AdminProducts/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: AdminProducts/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: AdminProducts/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
