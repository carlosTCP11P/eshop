﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eshop.Application;
using eshop.Application.Interface;
using eshop.Core;

namespace eshop.Web.Controllers
{
    public class ProductsController : Controller
    {

        private IProductsBL ProductsBL { get; set; } //iniciamos la interfaz de la clase, declarada la variable 


        public ProductsController(IProductsBL productsBL) //nueva instancia igualada a la antigua
        {
            ProductsBL = productsBL;
        } 
            
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }


        // GET: Product/Details/5
        public ActionResult Details(int id)
        {
            var r= ProductsBL.GetAllProducts();
            return View();
        }

        // GET: Product/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetAll()
        {
          var lstProducts = ProductsBL.GetAllProducts();
          return PartialView("_ProductsPartial", lstProducts);
        }


        // POST: Product/Create
        [HttpPost]
        public ActionResult PostProduct(Product productData)
        {

            ProductsBL.CreateProduct(productData);
            return View(); 

        }

        [HttpPut]
        public ActionResult UpdateProduct(Product product)
        {
            ProductsBL.UpdateProduct(product);
            return null;
        }

        // GET: Product/Edit/
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Product/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Product/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Product/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
