﻿using eshop.Application.Interface;
using eshop.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eshop.Web.Controllers
{
    public class HomeController : Controller
    {
        public IOrdersBL OrdersBL { get; set; }
        public HomeController(IOrdersBL ordersBL)
        {
            OrdersBL = ordersBL;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        public ActionResult GetCart()
        {
            Order orderSesion = (Order)Session["order"];
            return PartialView("_PartialCartDesc", orderSesion);
        }
    }
}