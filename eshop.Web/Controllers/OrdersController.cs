﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eshop.Application.Interface;
using eshop.Core;
using Microsoft.AspNet.Identity;

namespace eshop.Web.Controllers
{
    public class OrdersController : Controller
    {

        public IOrdersBL OrdersBL { get; set; }//declaramos la variable 
        public ILineOfOrdersBL LineOfOrdersBL { get; set; }


        public OrdersController(IOrdersBL ordersBL, ILineOfOrdersBL lineOfOrdersBL) { //inicializamos la variable 
            OrdersBL = ordersBL;
            LineOfOrdersBL = lineOfOrdersBL;

        }

        [HttpGet]
        public ActionResult GetByUser()
        {
            var userId = User.Identity.GetUserId();
            var result = OrdersBL.GetOrdersByUser(userId);
            return PartialView("_PartialPageOrdersByUser", result);
        }

        // GET: Orders
        public ActionResult Index()
        {
        
            return View();
        }

        public ActionResult EndOrder()
        {
            if (User.Identity.GetUserId() == null)
            {
                return RedirectToAction("Index", "Home");
            }
            Order orderSesion = (Order)Session["order"];
            return View(orderSesion);
        }

        // GET: Orders/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateOrder(Order order, int productId)
        {
            var user = User.Identity.GetUserId();
            if (user == null)
            {
                var url = new UrlHelper(Request.RequestContext).Action("Login", "Account");
                return Json(new { redirecturl = url });
            }

            order.UserId = user;
            order = OrdersBL.CreateOrder(order);

            LineOfOrder lineOfOrder = new LineOfOrder()
            {
                OrderId = order.Id,
                ProductId = productId
            };
            lineOfOrder=LineOfOrdersBL.CreateLine(lineOfOrder);
            order.LineOfOrder.Add(lineOfOrder);
            Session["order"] = order;
            return Json(new { count= order.LineOfOrder.Count});
        }

        [HttpPut]
        public ActionResult UpdateOrder(int productId)
        {
            Order order = (Order)Session["order"];
            var user = User.Identity.GetUserId();
            if (user == null)
            {
                var url = new UrlHelper(Request.RequestContext).Action("Login", "Account");
                return Json(new { redirecturl = url });
            }
            LineOfOrder lineOfOrder=null;
            for (int i=0; i < order.LineOfOrder.Count; i++)
            {
                if (order.LineOfOrder.ToList()[i].ProductId==productId)
                {
                    lineOfOrder = order.LineOfOrder.ToList()[i];
                    lineOfOrder.Quantity++;
                    order.LineOfOrder.ToList()[i] = LineOfOrdersBL.UpdateLine(lineOfOrder);
                    break;
                }
            }

            if (lineOfOrder==null)
            {
                lineOfOrder = new LineOfOrder()
                {
                    OrderId = order.Id,
                    ProductId = productId
                };
                lineOfOrder=LineOfOrdersBL.CreateLine(lineOfOrder);
                order.LineOfOrder.Add(lineOfOrder);
            }
            Session["order"] = order;
            return Json(new { count = order.LineOfOrder.Count });

        }

        [HttpPut]
        public ActionResult UpdateLine(int id, int quantity)
        {
            Order order = (Order)Session["order"];
            LineOfOrder lineOfOrder = order.LineOfOrder.FirstOrDefault(x => x.Id == id);
            lineOfOrder.Quantity = quantity;
            lineOfOrder.DateLOrderModification = DateTime.Now;
            Session["order"] = order;
            return Json(new { });
        }

        [HttpPut]
        public ActionResult FinishOrder()
        {
            Order order = (Order)Session["order"];
            OrdersBL.UpdateOrder(order);

            Session.Remove("order");
            var url = new UrlHelper(Request.RequestContext).Action("Index", "Home");
            return Json(new { redirecturl = url });
        }

        [HttpDelete]
        public ActionResult DeleteLine(int id)
        {
            Order order = (Order)Session["order"];
            LineOfOrdersBL.DeleteLine(id);
            LineOfOrder lineOfOrder = order.LineOfOrder.SingleOrDefault(x => x.Id == id);
            order.LineOfOrder.Remove(lineOfOrder);
            Session["order"] = order;
            return Json(new { count = order.LineOfOrder.Count });
        }

        // POST: Orders/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Orders/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Orders/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Orders/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Orders/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
