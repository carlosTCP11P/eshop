﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eshop.Core;
using eshop.Web.Models;

namespace eshop.Web.Controllers
{
    public class ShopController : Controller
    {
        private List<Product> Products { get; set; }


        public ShopController()
        {

            //    Products = new List<Product>
            //        {
            //            new Product { Id = 1, Name = "Producto 1", Description = "Producto", PriceProduct = 3.30, Stock=1, Image = "2.jpg", },
            //            new Product { Id = 2, Name = "Producto 2", Description = "Producto", PriceProduct = 3.30, Stock=2, Image = "2.jpg",  },
            //            new Product { Id = 3, Name = "Producto 3", Description = "Producto", PriceProduct = 3.30, Stock=0, Image = "3.jpg",  },
            //            new Product { Id = 4, Name = "Producto 4", Description = "Producto", PriceProduct = 3.30, Stock=4, Image = "4.jpg", },
            //            new Product { Id = 5, Name = "Producto 5", Description = "Producto", PriceProduct = 3.30, Stock=0, Image = "5.jpg",  },
            //            new Product { Id = 6, Name = "Producto 6", Description = "Producto", PriceProduct = 3.30, Stock=6, Image = "6.jpg", },
            //            new Product { Id = 7, Name = "Producto 7", Description = "Producto", PriceProduct = 3.30, Stock=7, Image = "7.jpg", },
            //            new Product { Id = 8, Name = "Producto 8", Description = "Producto", PriceProduct = 3.30, Stock=0, Image = "8.jpg", },
            //            new Product { Id = 9, Name = "Producto 9", Description = "Producto", PriceProduct = 3.30, Stock=9, Image = "9.jpg", },
            //        };
        }

        //Las imagenes maddre mia, y nuestra id ded linea de pedidos

            

        /////////////////////////////////////           
        // GET: Shop
        public ActionResult Index()
        {
            return View();
        }

        // GET: Shop/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Shop/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Shop/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Shop/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Shop/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Shop/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Shop/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
