namespace eshop.Core
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    /// <summary>
    /// Imagen ded producto
    /// </summary>
    [Table("Image")]
    public partial class Image
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        public int ProdctId { get; set; }

        public virtual Product Product { get; set; }
    }
}
