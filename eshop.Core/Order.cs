namespace eshop.Core
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    /// <summary>
    /// Creacion de nuestra clase parcial Pedido
    /// </summary>
    [Table("Order")]
    public partial class Order
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Order()
        {
            LineOfOrder = new HashSet<LineOfOrder>();
        }

        public int Id { get; set; }

        public DateTime DateOrderCreation { get; set; }

        [Required]
        [StringLength(128)]
        public string UserId { get; set; }

        public int StateId { get; set; }

        public DateTime? DateOrderModification { get; set; }

        public virtual AspNetUsers AspNetUsers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LineOfOrder> LineOfOrder { get; set; }

        public virtual StateType StateType { get; set; }
    }
}
