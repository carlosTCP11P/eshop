namespace eshop.Core
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    /// <summary>
    /// Creacion de Usuario bbdd
    /// </summary>
    [Table("User")]
    public partial class User
    {
        public string Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(100)]
        public string Passwd { get; set; }

        [Required]
        [StringLength(100)]
        public string Email { get; set; }

        public bool Admin { get; set; }

        [Required]
        [StringLength(50)]
        public string CardNumber { get; set; }

        [Required]
        [StringLength(50)]
        public string CardDate { get; set; }

        public int CardCVV { get; set; }
    }
}
