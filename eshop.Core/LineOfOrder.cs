namespace eshop.Core
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    /// <summary>
    /// Linea de pedidos 
    /// </summary>
    [Table("LineOfOrder")]
    public partial class LineOfOrder //clase de linea de pedidos con datos en tabla
    {
        public int Id { get; set; } //id

        public int OrderId { get; set; } //pedido id

        public DateTime DateLOrderCreation { get; set; } //Creacion fecha

        public DateTime? DateLOrderModification { get; set; } //Creacion modificacion

        public int ProductId { get; set; } //ProductoId

        public int Quantity { get; set; } //Cantidad id 

        public bool Activated { get; set; } //Dentro

        public double Price { get; set; } //Precio

        public virtual Order Order { get; set; } //PEDIDO

        public virtual Product Product { get; set; } //PRODUCTO
    }
}
