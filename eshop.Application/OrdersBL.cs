﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eshop.Application.Interface;
using eshop.Core;
using eshop.DAL;
using static eshop.DAL.eshopENUM;

namespace eshop.Application
{
    public class OrdersBL : BaseWorker ,IOrdersBL
    {
        /// <summary>
        /// Iniciamos repos
        /// </summary>
    
        private static IRepository<Order> OrdersRepository; //inicar el repositorio con la clase que necesitamos, en este caso Pedidos
        private static IRepository<LineOfOrder> LineOfOrdersRepository; //iniciamos el repositorio para lineas de pedido 
        private static IRepository<Product> ProductsRepository; //inciamos repo productos
        //no tenemos valores en las variables,esta declarado 

         /// <summary>
         /// al padre le pasamos por parametro uow, Pedido
         /// </summary>
          
        public OrdersBL(IUnitOfWork uow) : base(uow) 
        {
            OrdersRepository = uow.GetRepository<Order>(); //Metodo para iniciar el repositorio, productos
            LineOfOrdersRepository = uow.GetRepository<LineOfOrder>(); //iniciamos nuestro repositorio, lienas de pedido 
            ProductsRepository = uow.GetRepository<Product>(); // inicamos el repo de productos con valor
            //aqui empezaran a tener valores
        }
        
        /// <summary>
        /// Lista de todos los pedidos
        /// </summary>
        /// <returns>Productos por id</returns> Consultas Linq
        public List<Order> GetAllOrders() //creamos un metodo para recuperar todos los productos, id
        {
            var result = OrdersRepository.GetAll(x=>x.StateId > 1, x=>x.AspNetUsers, x => x.LineOfOrder, x => x.LineOfOrder.Select(y=>y.Product)).ToList(); 
            return result;
        } 
        /// <summary>
        /// Pedido
        /// </summary>
        /// <param name="id"> id del producto</param>
        /// <returns> todas lineas de pedido por Id</returns>
        public Order GetById(int id)
        {
            var result = OrdersRepository.GetAll(x => x.Id == id, x => x.LineOfOrder, x => x.LineOfOrder.Select(y => y.Product)).FirstOrDefault();
            return result;
        }
        /// <summary>
        /// Lista de pedidos
        /// </summary>
        /// <param name="id"> Lista peddiso por id</param>
        /// <returns></returns>
        public List<Order> GetOrdersByUser(string id)
        {
            var result = OrdersRepository.GetAll(x => x.UserId == id && x.StateId>1, y => y.LineOfOrder).ToList();
            return result;
        }
        /// <summary>
        /// Creacion de pediso
        /// </summary>
        /// <param name="order">fecha, estado y repo</param>
        /// <returns></returns>
        public Order CreateOrder(Order order)
        {
            order.DateOrderCreation = DateTime.Now;
            order.StateId = (int)TypeState.CreandoPedido;
            order = OrdersRepository.Create(order); 
            uow.Save();

            return order;
            //Creamos nuestras lineas de pedido en bbdd 
        }
        /// <summary>
        /// Update de nuestro pedido
        /// </summary>
        /// <param name="order">Linea de peidido tmbn update</param>
        public void UpdateOrder(Order order)
        {
            foreach (var item in order.LineOfOrder)
            {
                LineOfOrdersRepository.Update(item);
                var prod= ProductsRepository.GetAll(x=>x.Id==item.ProductId).FirstOrDefault();
                prod.Stock -= item.Quantity;
                ProductsRepository.Update(prod);
            }

            order.DateOrderModification = DateTime.Now;
            order.StateId = (int)TypeState.PedidoRealizado;
            OrdersRepository.Update(order);

            uow.Save();
        }
        /// <summary>
        /// Borramos pedido
        /// </summary>
        /// <param name="Id">Mediante id delete</param>
        public void DeleteOrder(int Id)
        {
            OrdersRepository.RemoveById(Id);
            uow.Save(); //Guardamos datos 
        }

    }
}
