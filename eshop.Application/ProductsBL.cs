﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eshop.Application.Interface;
using eshop.Core;
using eshop.DAL;

namespace eshop.Application
{
    public class ProductsBL : BaseWorker, IProductsBL
    {/// <summary>
    /// Declaramos el repositorio que usaremos
    /// </summary>
        private static IRepository<Product> ProductsRepository; //Declaramos el repositorio

        /// <summary>
        /// Metodo para inicializar el repo de productos
        /// </summary>
        /// <param name="uow">Pasamos por parametro uow</param>
        public ProductsBL(IUnitOfWork uow) : base(uow) // al padre le estamos pasando por parametro uow
        { //aqui la inicializamos
            ProductsRepository = uow.GetRepository<Product>(); 
        }
        /// <summary>
        /// Recuperar todos los productos
        /// </summary>
        /// <returns>Lista de productos</returns>
        public List<Product> GetAllProducts() 
        {
            var result = ProductsRepository.GetAll(null, prod => prod.Image).ToList();//Nos devuelve una lista, esta nos vuelve sin su relaciones => pero las añadimos en la consulta     
            return result;
        }
        /// <summary>
        /// Metodod para la crear productos
        /// </summary>
        /// <param name="product">Id del producto a Crear</param>
        public void CreateProduct(Product product) // metodo para crear
        {
            ProductsRepository.Create(product);
            uow.Save(); //Guardamos datos 
        }

        /// <summary>
        /// Metodo para actualizar productos
        /// </summary>
        /// <param name="product">Id del producto a Actualizar</param>
        public void UpdateProduct(Product product)
        {
            ProductsRepository.Update(product);
            uow.Save();
        }

        /// <summary>
        /// Metodo para borrar productos
        /// </summary>
        /// <param name="Id">Id del producto a Borrar</param>
        public void DeleteProduct (int Id)
        {
            ProductsRepository.RemoveById(Id);
            uow.Save(); //Guardamos datos 
        }
        
    }
}
