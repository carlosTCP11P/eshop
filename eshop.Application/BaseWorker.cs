﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eshop.DAL;

namespace eshop.Application
{
    public class BaseWorker 
    {/// <summary>
    /// Clase Padre
    /// </summary>
        public IUnitOfWork uow;

        public BaseWorker(IUnitOfWork Uow) //pasamos la interfaz por parametro CONSTRUCTOR 
        {
            uow = Uow;
        }

    }
}
