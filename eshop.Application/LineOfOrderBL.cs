﻿using eshop.Application.Interface;
using eshop.Core;
using eshop.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eshop.Application
{
    public class LineOfOrderBL : BaseWorker, ILineOfOrdersBL //heredamos de nuestra clase padre y la interfaz 
    {/// <summary>
    /// Linea de pedido y producto igualado 
    /// </summary>
        private static IRepository<LineOfOrder> LineOfOrdersRepository;///
        private static IRepository<Product> ProductsRepository;
        public LineOfOrderBL(IUnitOfWork Uow) : base(Uow)
        {
            LineOfOrdersRepository = Uow.GetRepository<LineOfOrder>();
            ProductsRepository = Uow.GetRepository<Product>();
        }
        /// <summary>
        /// Creacion de linea de pedido 
        /// </summary>
        /// <param name="lineOfOrder">  </param>
        /// <returns>Linea de pedido</returns>
        public LineOfOrder CreateLine(LineOfOrder lineOfOrder)
        {
            Product product = ProductsRepository.GetAll(x => x.Id == lineOfOrder.ProductId, x => x.Image).FirstOrDefault();
            lineOfOrder.Price = product.PriceProduct;
            lineOfOrder.Quantity = 1;
            lineOfOrder.Activated = true;
            lineOfOrder.DateLOrderCreation = DateTime.Now;
            lineOfOrder = LineOfOrdersRepository.Create(lineOfOrder);
            uow.Save();
            lineOfOrder.Product = product;
            return lineOfOrder;
        }
        /// <summary>
        /// Modificacion de Linea de pedido 
        /// </summary>
        /// <param name="lineOforder">Creación y update</param>
        /// <returns>Linea de pedido</returns>
        public LineOfOrder UpdateLine(LineOfOrder lineOforder)
        {
            lineOforder.DateLOrderModification = DateTime.Now;
            LineOfOrdersRepository.Update(lineOforder);
            uow.Save();

            return lineOforder;
        }
        /// <summary>
        /// Borramos linea de pedido
        /// </summary>
        /// <param name="id">Borramos por Id</param>
        public void DeleteLine(int id)
        { 
            LineOfOrdersRepository.RemoveById(id);
            uow.Save();
        }
    }
}
