﻿using eshop.Application.Interface;
using eshop.Core;
using eshop.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eshop.Application
{
    public class UsersBL : BaseWorker, IUsersBL
    {/// <summary>
    /// Usuarios del repo
    /// </summary>
        private static IRepository<AspNetUsers> UsersRepository; 

        /// <summary>
        /// Inicializamos el repo
        /// </summary>
        /// <param name="Uow">pasamos uow</param>
        public UsersBL(IUnitOfWork Uow) : base(Uow)
        {
            UsersRepository= Uow.GetRepository<AspNetUsers>();
        }
        /// <summary>
        /// Creacion de rol de usuario
        /// </summary>
        /// <param name="id">Exp</param>
        public void CreateRolNewUser(string id)
        {
            throw new NotImplementedException(); //Excepcion
        }
        /// <summary>
        /// Lista de todos los usuarios
        /// </summary>
        /// <returns>Coleccion de roles de usuario</returns>
        public List<AspNetUsers> GetAllUsers()
        {
            var users = UsersRepository.GetAll(null,x=>x.AspNetRoles).ToList();
            return users;
        }
    }
}
