﻿using eshop.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eshop.Application.Interface
{

    public interface ILineOfOrdersBL
    {/// <summary>
    /// Interfaz de nuestra linea de pedido
    /// </summary>
    /// <param name="lineOfOrder"></param>
    /// <returns>Linea de pedido pasado por parametro</returns>
        LineOfOrder CreateLine(LineOfOrder lineOfOrder); //crear

        LineOfOrder UpdateLine(LineOfOrder lineOfOrder); //updatear

        void DeleteLine(int id); //borrar con id 
    }
}
