﻿using eshop.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eshop.Application.Interface
{
    public interface IUsersBL
    {/// <summary>
    /// Interfaz Usuario
    /// </summary>
    /// <returns></returns>
        List<AspNetUsers> GetAllUsers(); //Lista usuarios
        void CreateRolNewUser(string id); //Crear nuevo rol usurio (id)
    }
}
