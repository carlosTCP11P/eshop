﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eshop.Core;
using eshop.DAL;

namespace eshop.Application.Interface
{
    public interface IOrdersBL
    {
        /// <summary>
        /// Declarar los metodos 
        /// </summary>
        /// <returns>sin valor aun</returns>
        List<Order> GetAllOrders();
        List<Order> GetOrdersByUser(string id);
        Order GetById(int id);
        Order CreateOrder(Order order);
        void UpdateOrder(Order order);
    }
}
