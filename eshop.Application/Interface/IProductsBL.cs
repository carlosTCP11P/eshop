﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eshop.Core;
using eshop.DAL;

namespace eshop.Application.Interface
{
    public interface IProductsBL
    {/// <summary>
    /// Interza de nuestros productos
    /// </summary>
    /// <returns></returns>
        List<Product> GetAllProducts();
        void CreateProduct(Product product);
        void UpdateProduct(Product product);
        void DeleteProduct(int id);
    }
}
