﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eshop.DAL
{/// <summary>
/// Interfaz de nuestro uow
/// </summary>
    public interface IUnitOfWork
    {
        IRepository<T> GetRepository<T>() where T : class;

        TRepository GetCustomRepository<TRepository>() where TRepository : IRepository;

        void Save();
    }
}
