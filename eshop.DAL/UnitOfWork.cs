﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eshop.DAL
{/// <summary>
///  UnitOfWork
/// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        private eshopContext dbContext;

        private readonly ConcurrentDictionary<Type, object> _repositories;

        public UnitOfWork(eshopContext context)
        {
            dbContext = context;
            _repositories = new ConcurrentDictionary<Type, object>();
        }

        public IRepository<T> GetRepository<T>() where T : class
        {
            return (Repository<T>)_repositories.GetOrAdd(typeof(T), new Repository<T>(this.dbContext));
        }

        public TRepository GetCustomRepository<TRepository>() where TRepository : IRepository
        {
            Type type = typeof(TRepository);
            Type implementacion = AppDomain.CurrentDomain.GetAssemblies().SelectMany(d => d.GetTypes()).Where(p => type.IsAssignableFrom(p)).FirstOrDefault();

            if (implementacion == null)
            {
                throw new NullReferenceException();
            }
            return (TRepository)Activator.CreateInstance(implementacion, dbContext);

        }

        public void Save()
        {
            try
            {
                dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                var changedEntries = dbContext.ChangeTracker.Entries()
                                     .Where(x => x.State != EntityState.Unchanged).ToList();

                foreach (var entry in changedEntries)
                {

                    entry.State = EntityState.Detached;
                }
                throw;
            }
        }

        public void Dispose()
        {
            dbContext.Dispose();
        }
    }
}

//Cada vez que iniciemos un repositorio, al UOW a registrarlo GETrepository