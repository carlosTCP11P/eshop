﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace eshop.DAL
{/// <summary>
/// Genery Repository
/// </summary>

    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly DbContext _context;
        public readonly DbSet<T> _entities;

        public Repository(eshopContext eshopContext)
        {
            _context = eshopContext;
            _entities = eshopContext.Set<T>();
        }

        public async Task<IEnumerable<T>> FindAll()
        {
            return await _entities.ToListAsync();
        }

        public T FindById(int id)
        {
            return _entities.Find(id);
        }

        public virtual ICollection<T> GetAll(Expression<Func<T, bool>> filter, params Expression<Func<T, object>>[] includes)
        {
            var result = QueryDb(filter, null, includes);
            return result.ToList();
        }

        public virtual T Get(int id, params Expression<Func<T, object>>[] includes)
        {


            IQueryable<T> query = _entities;

            foreach (var property in includes)
            {
                query = query.Include(property);

            }

            return query.SingleOrDefault(x => x.GetType().GetProperty("Id").GetValue(x, null).Equals(id));

        }

        public virtual T Create(T entity)
        {
            if (entity == null) throw new InvalidOperationException("Unable to add a null entity to the repository.");
            return _entities.Add(entity);
        }

        public virtual void Update(T entity)
        {
            //_entities.Find(entity.Id);
            //_context.Entry(entity).CurrentValues.SetValues(entity);
            _entities.AddOrUpdate(entity);
            //return _entities.Attach(entity);
        }

        public virtual void Remove(T entity)
        {
            _entities.Remove(entity);
        }

        public virtual void RemoveById(int id)
        {
            var entity = _entities.Find(id);
            _entities.Remove(entity);
        }

        protected IQueryable<T> QueryDb(Expression<Func<T, bool>> filter, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy, params Expression<Func<T, object>>[] includes)
        {
            IQueryable<T> query = _entities;
            query = query.AsNoTracking();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (includes != null)
            {
                foreach (var property in includes)
                {
                    query = query.Include(property);

                }
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            return query;
        }

    }
}
