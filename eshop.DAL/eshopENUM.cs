﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eshop.DAL
{/// <summary>
/// Enumerado
/// </summary>
    public static class eshopENUM
    {/// <summary>
    /// Tipo de estado de nuestro pedido
    /// </summary>
        public enum TypeState
        {
            CreandoPedido=1,
            PedidoRealizado=2,
            PedidoPagado=3,
            PedidoEnviado = 4,
            PedidoRecibido = 5,
            ErrorPedido=6
        }
    }
}
