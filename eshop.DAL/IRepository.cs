﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace eshop.DAL
{/// <summary>
/// Interfaz de nuestro repositorio con sus propiedades
/// </summary>
    public interface IRepository { }
    public interface IRepository<T> : IRepository where T : class
    {
        ICollection<T> GetAll(Expression<Func<T, bool>> filter, params Expression<Func<T, object>>[] includes);
        T Get(int id, params Expression<Func<T, object>>[] includes);
        T Create(T entity);
        void Update(T entity);
        void Remove(T entity);
        void RemoveById(int id);
    }
}
